package ${package.Entity};

<#list table.importPackages as pkg>
<#if pkg == "java.util.Date">
import java.util.Date;
</#if>
<#if pkg == "java.time.LocalDate">
import java.time.LocalDate;
</#if>
<#if pkg == "java.time.LocalDateTime">
import java.time.LocalDateTime;
</#if>
<#if pkg == "java.math.BigDecimal">
import java.math.BigDecimal;
</#if>
</#list>
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;


/**
 * <p>
 * ${table.comment!}
 * </p>
 * @author ${author}
 * @since ${date}
 */
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ApiModel(value = "${table.name}", description = "${table.comment!}")
public class ${entity}DTO {

<#list table.fields as field>

    /**
    * ${field.comment}
    */
    <#if field.keyFlag>
        <#assign keyPropertyName="${field.propertyName}"/>
    </#if>
    <#if field.comment!?length gt 0>
    <#if swagger>
    @ApiModelProperty("${field.comment}")
    <#else>
        /**
         * ${field.comment}
         */
    </#if>
    </#if>
    private ${field.propertyType} ${field.propertyName};
</#list>

<#if entityColumnConstant>
    <#list table.fields as field>
        public static final String ${field.name?upper_case} = "${field.name}";
    </#list>
</#if>
<#if activeRecord>
    @Override
    public Serializable pkVal() {
    <#if keyPropertyName??>
        return this.${keyPropertyName};
    <#else>
        return null;
    </#if>
    }

</#if>
<#if !entityLombokModel>
    @Override
    public String toString() {
    return "${entity}{" +
    <#list table.fields as field>
        <#if field_index==0>
            "${field.propertyName}=" + ${field.propertyName} +
        <#else>
            ", ${field.propertyName}=" + ${field.propertyName} +
        </#if>
    </#list>
    "}";
    }
</#if>
}