package ${package.Service};

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import ${package.Entity}.${entity};
import com.baomidou.mybatisplus.extension.service.IService;

/**
*  Service接口 : ${table.comment!}
*  @author ${author}
*  @since ${date}
*/
public interface ${table.serviceName} extends IService<${entity}> {
    /**
     * 分页查询${table.comment}信息
     */
    Page<${entity}> select${entity}ListByPage(${entity}VO ${table.entityPath}VO);

    /**
     * 根据id查询${table.comment}信息
     */
    ${entity} select${entity}ById(${entity}VO ${table.entityPath}VO);
}