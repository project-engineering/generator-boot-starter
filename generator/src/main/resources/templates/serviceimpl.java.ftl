package ${package.ServiceImpl};

import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import ${package.Entity}.${entity};
import ${package.Mapper}.${table.mapperName};
import ${package.Service}.${table.serviceName};
import com.baomidou.mybatisplus.core.conditions.query.LambdaQueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;
import jakarta.annotation.Resource;
import java.util.List;

/**
 *  Service业务逻辑处理 : ${table.comment}
 *  @author ${author}
 *  @since ${date}
 */
@Service
public class ${table.serviceImplName} extends ServiceImpl<${table.mapperName}, ${entity}> implements ${table.serviceName} {
    @Resource
    private ${table.mapperName} ${table.entityPath}Mapper;

    /**
     * 根据id查询二代公共信息段表信息
     */
    public Page<${entity}> select${entity}ListByPage(${entity}VO ${table.entityPath}VO){
        Page<${entity}> page = new Page<>(${table.entityPath}VO.getPageNum,${table.entityPath}VO.getPageSize);
        //queryWrapper组装查询where条件
        LambdaQueryWrapper<${entity}> queryWrapper = new LambdaQueryWrapper<>();
        return ${table.entityPath}Mapper.selectPage(page,queryWrapper);
    }

    /**
     * 根据id查询${table.comment}信息
     */
    public ${entity} select${entity}ById(${entity}VO ${table.entityPath}VO){
        return ${table.entityPath}Mapper.selectById(${table.entityPath}VO);
    }

}