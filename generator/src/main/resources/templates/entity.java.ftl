package ${package.Entity};

import com.baomidou.mybatisplus.annotation.*;
import java.io.Serializable;
<#list table.importPackages as pkg>
<#if pkg == "java.util.Date">
import java.util.Date;
</#if>
<#if pkg == "java.time.LocalDate">
import java.time.LocalDate;
</#if>
<#if pkg == "java.time.LocalDateTime">
import java.time.LocalDateTime;
</#if>
<#if pkg == "java.math.BigDecimal">
import java.math.BigDecimal;
</#if>
</#list>
import com.fasterxml.jackson.annotation.JsonFormat;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.*;
import org.hibernate.validator.constraints.Length;
import jakarta.validation.constraints.NotEmpty;
import jakarta.validation.constraints.NotNull;


/**
* <p>
* ${table.comment!}
* </p>
* @author ${author}
* @since ${date}
*/
@Data
@Builder
@AllArgsConstructor
@NoArgsConstructor
@TableName("${table.name}")
@ApiModel(value = "${table.name}对象", description = "${table.comment!}")
public class ${entity} implements Serializable {

<#list table.fields as field>

    /**
    * ${field.comment}
    */
<#-- 根据数据库字段类型判断Java数据类型 -->
<#if field.propertyType == 'LocalDateTime'>
    @JsonDeserialize(using = LocalDateTimeDeserializer.class)
    @JsonSerialize(using = LocalDateTimeSerializer.class)
</#if>
<#if field.propertyType == 'String'>
    @NotEmpty(message = "${field.annotationColumnName} could not be empty")
    @Length(max = ${field?size - 1}, message = "${field.annotationColumnName} length could not exceed ${field?size - 1}")
</#if>
<#if field.keyFlag>
<#assign keyPropertyName="${field.propertyName}"/>
</#if>
<#if field.comment!?length gt 0>
<#if swagger>
    @ApiModelProperty("${field.comment}")
<#else>
/**
* ${field.comment}
*/
</#if>
</#if>
<#if field.keyFlag>
<#-- 主键 -->
<#if field.keyIdentityFlag>
    @TableId(value = "${field.annotationColumnName}", type = IdType.AUTO)
    @NotNull(message = "${field.annotationColumnName} could not be null", groups = {EditValidationGroup.class})
<#elseif idType??>
    @TableId(value = "${field.annotationColumnName}", type = IdType.${idType})
    @NotNull(message = "${field.annotationColumnName} could not be null")
<#elseif field.convert>
    @TableId("${field.annotationColumnName}")
    @NotNull(message = "${field.annotationColumnName} could not be null")
</#if>
<#-- 普通字段 -->
<#elseif field.fill??>
<#-- -----   存在字段填充设置   ----->
<#if field.convert>
    @TableField(value = "${field.annotationColumnName}", fill = FieldFill.${field.fill})
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
<#else>
    @TableField(fill = FieldFill.${field.fill})
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss",timezone="GMT+8")
</#if>
<#elseif field.convert>
    @TableField("${field.annotationColumnName}")
</#if>
<#-- 乐观锁注解 -->
<#if field.versionField>
    @Version
</#if>
<#-- 逻辑删除注解 -->
<#if field.logicDeleteField>
    @TableLogic
</#if>
    private ${field.propertyType} ${field.propertyName};
</#list>

<#if entityColumnConstant>
    <#list table.fields as field>
        public static final String ${field.name?upper_case} = "${field.name}";

    </#list>
</#if>
<#if activeRecord>
    @Override
    public Serializable pkVal() {
    <#if keyPropertyName??>
        return this.${keyPropertyName};
    <#else>
        return null;
    </#if>
    }

</#if>
<#if !entityLombokModel>
    @Override
    public String toString() {
    return "${entity}{" +
    <#list table.fields as field>
        <#if field_index==0>
            "${field.propertyName}=" + ${field.propertyName} +
        <#else>
            ", ${field.propertyName}=" + ${field.propertyName} +
        </#if>
    </#list>
    "}";
    }
</#if>
}