package com.springboot.generator.service.basic.impl;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.generator.common.constant.Constant;
import com.springboot.generator.entity.basic.SysMenu;
import com.springboot.generator.entity.basic.SysRole;
import com.springboot.generator.entity.basic.SysUser;
import com.springboot.generator.mapper.basic.SysMenuMapper;
import com.springboot.generator.mapper.basic.SysRoleMapper;
import com.springboot.generator.mapper.basic.SysUserMapper;
import com.springboot.generator.service.basic.SysUserService;
import com.springboot.generator.util.RedisUtil;
import com.springboot.generator.util.StringUtil;
import jakarta.annotation.Resource;
import org.springframework.stereotype.Service;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-02-01
 */
@Service
public class SysUserServiceImpl extends ServiceImpl<SysUserMapper, SysUser> implements SysUserService {
    @Resource
    private SysRoleMapper sysRoleMapper;

    @Resource
    private SysUserMapper sysUserMapper;

    @Resource
    private SysMenuMapper sysMenuMapper;

    @Resource
    private RedisUtil redisUtil;

    @Override
    public SysUser getByUserName(String username) {
        return getOne(new QueryWrapper<SysUser>().eq("username",username));
    }

    @Override
    public String getUserAuthorityInfo(Long userId) {
        StringBuffer authority=new StringBuffer();

        if(redisUtil.hasKey(Constant.AUTHORITY_KEY+userId)){
            System.out.println("有缓存");
            authority.append(redisUtil.get(Constant.AUTHORITY_KEY,String.valueOf(userId)));
        }else{
            System.out.println("没缓存");
            // 获取角色
            List<SysRole> roleList = sysRoleMapper.selectList(new QueryWrapper<SysRole>().inSql("id", "select role_id from sys_user_role where user_id=" + userId));

            if(roleList.size()>0){
                String roleCodeStrs=roleList.stream().map(r->"ROLE_"+r.getCode()).collect(Collectors.joining(","));
                authority.append(roleCodeStrs);
            }

            // 获取菜单权限
            Set<String> menuCodeSet = new HashSet<String>();
            for(SysRole sysRole:roleList){
                List<SysMenu> sysMenuList = sysMenuMapper.selectList(new QueryWrapper<SysMenu>().inSql("id", "select menu_id from sys_role_menu where role_id=" + sysRole.getId()));
                for(SysMenu sysMenu:sysMenuList){
                    String perms=sysMenu.getPerms();
                    if(StringUtil.isNotEmpty(perms)){
                        menuCodeSet.add(perms);
                    }
                }
            }
            if(menuCodeSet.size()>0){
                authority.append(",");
                String menuCodeStrs = menuCodeSet.stream().collect(Collectors.joining(","));
                authority.append(menuCodeStrs);
            }
            redisUtil.set(Constant.AUTHORITY_KEY,String.valueOf(userId),authority,10*60);
            System.out.println("authority:"+authority.toString());
        }
        return authority.toString();
    }
}
