package com.springboot.generator.service.basic;

import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.generator.entity.basic.SysMenu;
import java.util.List;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuc
 * @since 2024-02-01
 */
public interface SysMenuService extends IService<SysMenu> {
    List<SysMenu> buildTreeMenu(List<SysMenu> sysMenuList);
}
