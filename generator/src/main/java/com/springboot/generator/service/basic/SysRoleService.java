package com.springboot.generator.service.basic;

import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.generator.entity.basic.SysRole;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuc
 * @since 2024-02-01
 */
public interface SysRoleService extends IService<SysRole> {

}
