package com.springboot.generator.service.basic.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.springboot.generator.entity.basic.SysRoleMenu;
import com.springboot.generator.mapper.basic.SysRoleMenuMapper;
import com.springboot.generator.service.basic.SysRoleMenuService;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author liuc
 * @since 2024-02-01
 */
@Service
public class SysRoleMenuServiceImpl extends ServiceImpl<SysRoleMenuMapper, SysRoleMenu> implements SysRoleMenuService {

}
