package com.springboot.generator.service.basic;

import com.baomidou.mybatisplus.extension.service.IService;
import com.springboot.generator.entity.basic.SysUser;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author liuc
 * @since 2024-02-01
 */
public interface SysUserService extends IService<SysUser> {
    SysUser getByUserName(String username);

    String getUserAuthorityInfo(Long userId);
}
