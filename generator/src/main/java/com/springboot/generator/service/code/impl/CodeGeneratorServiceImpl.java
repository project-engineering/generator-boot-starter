package com.springboot.generator.service.code.impl;

import com.baomidou.mybatisplus.annotation.FieldFill;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.FastAutoGenerator;
import com.baomidou.mybatisplus.generator.config.OutputFile;
import com.baomidou.mybatisplus.generator.config.TemplateConfig;
import com.baomidou.mybatisplus.generator.config.TemplateType;
import com.baomidou.mybatisplus.generator.config.rules.DateType;
import com.baomidou.mybatisplus.generator.engine.FreemarkerTemplateEngine;
import com.baomidou.mybatisplus.generator.fill.Column;
import com.springboot.generator.entity.code.ColumnDetail;
import com.springboot.generator.mapper.code.CodeGeneratorMapper;
import com.springboot.generator.service.code.ICodeGeneratorService;
import jakarta.annotation.Resource;
import lombok.extern.log4j.Log4j2;
import org.apache.velocity.VelocityContext;
import org.apache.velocity.app.Velocity;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import java.io.StringWriter;
import java.util.*;
import java.util.function.Consumer;

/**
 * @author liuc
 * @apiNote 代码生成器业务层
 * @date 2024/1/17 13:22
 * @tool Created by IntelliJ IDEA
 */
@Log4j2
@Service
public class CodeGeneratorServiceImpl implements ICodeGeneratorService {
    //    数据库url
    public static final String MYSQL_URL = "jdbc:mysql://localhost:3306/gp_credit?serverTimezone=GMT%2B8";
    //    数据库登录名
    public static final String MYSQL_UAERNAME = "root";
    //    数据库密码
    public static final String MYSQL_PASSWORD = "123456";
    //    代码 作者
    public static final String AUTHOR = "liuc";
    //    父类包名
    public static final String PARENT_PACKAGE = "com.springboot.credit.basic";
    //    需要生成代码的数据表名
    public static final String TABLE_NAME = "ci_2g_packet_file";
    // controller的模板路径
    public static final String CONTROLLER_TEMPLATE = "/templates/controller.java";
    //  mapper的模板路径
    public static final String MAPPER_TEMPLATE = "/templates/mapper.java";
    // mapper.xml模板的路径
    public static final String MAPPERXML_TEMPLATE = "/templates/mapper.xml";
    // entity的模板路径
    public static final String ENTITY_TEMPLATE = "/templates/entity.java";
    //获取当前工程路径(这里无需修改)
    private static String PROJECT_ROOT_PATH = System.getProperty("user.dir");
    //MapperXml 路径
    private static String MAPPER_XML_PATH = PROJECT_ROOT_PATH + "/src/main/resources/mapper";

    @Value("${cn.molu.generate.database}")
    private String database; // 这里是数据库的名字，我的数据库是 temp1
    @Resource
    CodeGeneratorMapper codeGeneratorMapper;

    /**
     * 获取数据库中所有表信息
     */
    public List<ColumnDetail> getAllTableInfo() {
        return codeGeneratorMapper.getColumnDetailMapVo(database);
    }

    /**
     * 获取表中所有字段信息
     *
     * @param tableName 表名
     */
    public List<ColumnDetail> getColumnDetails(String tableName) {
        List<String> column = codeGeneratorMapper.getColumnDetail(tableName);
        return codeGeneratorMapper.getColumnDetailMapVoByTableName(database, tableName);
    }

    /**
     * 生成代码
     *
     * @param columnDetailList 表字段信息
     * @param delPrefix        需要去除的前缀（tb_）
     * @param packageName      文件所在包（cn.molu.generator）
     * @param type             生成类型（java/entity.java、vue3/vue2Page.vue3...）
     */
    public String generateCode(List<ColumnDetail> columnDetailList, String delPrefix, String packageName, String type) {
        try (StringWriter writer = new StringWriter()) {
            Properties properties = new Properties();
            properties.setProperty("resource.loader.file.class", "org.apache.velocity.runtime.resource.loader.ClasspathResourceLoader");
            properties.setProperty(Velocity.INPUT_ENCODING, "UTF-8");
            Velocity.init(properties);
            Map<String, Object> map = new ColumnDetail().listToMap(columnDetailList, delPrefix);
            map.put("packageName", packageName);
            VelocityContext context = new VelocityContext();
            context.put("map", map);
            Velocity.getTemplate("vm/" + type + ".vm", "UTF-8").merge(context, writer);
            return writer.toString();
        } catch (Exception e) {
            log.error("报错了：" + e.getMessage(), e);
            return "报错了：" + e.getMessage();
        }
    }

    public void generateCode (){
        // 数据源配置
        FastAutoGenerator.create(MYSQL_URL, MYSQL_UAERNAME, MYSQL_PASSWORD)
                .globalConfig(builder -> {
                    // 设置作者
                    builder.author(AUTHOR)
                            // 开启 swagger 模式 默认值:false
                            .enableSwagger()
                            // 禁止打开输出目录 默认值:true
                            .disableOpenDir()
                            // 注释日期
                            .commentDate("yyyy-MM-dd")
                            //定义生成的实体类中日期类型 DateType.ONLY_DATE 默认值: DateType.TIME_PACK
                            .dateType(DateType.ONLY_DATE)
                            // 指定输出目录
                            .outputDir(PROJECT_ROOT_PATH + "/src/main/java");
                })

                .packageConfig(builder -> {
                    builder.parent(PARENT_PACKAGE) // 父包模块名
                            .controller("controller")   //Controller 包名 默认值:controller
                            .entity("entity")           //Entity 包名 默认值:entity
                            .service("service")         //Service 包名 默认值:service
                            .mapper("mapper")           //Mapper 包名 默认值:mapper
//                            .other("model")
                            //.moduleName("xxx")        // 设置父包模块名 默认值:无
                            // 设置mapperXml生成路径//默认存放在mapper的xml下
                            .pathInfo(Collections.singletonMap(OutputFile.xml, MAPPER_XML_PATH));
                })

                .injectionConfig(consumer -> {
                    Map<String, String> customFile = new HashMap<>();
                    // DTO、VO
                    customFile.put("DTO.java", "/templates/entityDTO.java.ftl");
                    customFile.put("VO.java", "/templates/entityVO.java.ftl");
                    consumer.customFile(customFile);
                })

                .strategyConfig(builder -> {
                    builder.addInclude(TABLE_NAME) // 设置需要生成的表名 可边长参数“user”, “user1”
                            .addTablePrefix("tb_", "ci_2g_") // 设置过滤表前缀
                            .serviceBuilder()//service策略配置
                            .formatServiceFileName("I%sService")
                            .formatServiceImplFileName("%sServiceImpl")
                            .entityBuilder()// 实体类策略配置
                            .idType(IdType.ASSIGN_ID)//主键策略  雪花算法自动生成的id
                            .addTableFills(new Column("create_time", FieldFill.INSERT)) // 自动填充配置
                            .addTableFills(new Column("update_time", FieldFill.INSERT_UPDATE))
                            .enableLombok() //开启lombok
                            .logicDeleteColumnName("deleted")// 说明逻辑删除是哪个字段
                            .enableTableFieldAnnotation()// 属性加上注解说明
                            .controllerBuilder() //controller 策略配置
                            .formatFileName("%sController")
                            .enableRestStyle() // 开启RestController注解
                            .mapperBuilder()// mapper策略配置
                            .enableMapperAnnotation()//@mapper注解开启
                            .formatXmlFileName("%sMapper")
                            .enableBaseColumnList()
                            .enableBaseResultMap();
                })
                .templateConfig(new Consumer<TemplateConfig.Builder>() {
                    @Override
                    public void accept(TemplateConfig.Builder builder) {
                        builder.disable(TemplateType.ENTITY)
                                .entity(ENTITY_TEMPLATE)
                                .controller("/templates/controller.java")
                                .service("/templates/service.java")
                                .serviceImpl("/templates/serviceImpl.java")
                                .mapper(MAPPER_TEMPLATE)
                                .xml(MAPPERXML_TEMPLATE);
                    }
                })

                // 使用Freemarker引擎模板，默认的是Velocity引擎模板
                .templateEngine(new FreemarkerTemplateEngine())
                .execute();

    }
}
