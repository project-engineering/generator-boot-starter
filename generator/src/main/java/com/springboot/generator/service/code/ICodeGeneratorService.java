package com.springboot.generator.service.code;

import com.springboot.generator.entity.code.ColumnDetail;

import java.util.*;

/**
 * @author liuc
 * @apiNote 代码生成器业务层
 * @date 2024/1/17 13:22
 * @tool Created by IntelliJ IDEA
 */
public interface ICodeGeneratorService {

    /**
     * 获取数据库中所有表信息
     */
    List<ColumnDetail> getAllTableInfo();

    /**
     * 获取表中所有字段信息
     *
     * @param tableName 表名
     */
    List<ColumnDetail> getColumnDetails(String tableName);

    /**
     * 生成代码
     *
     * @param columnDetailList 表字段信息
     * @param delPrefix        需要去除的前缀（tb_）
     * @param packageName      文件所在包（cn.molu.generator）
     * @param type             生成类型（java/entity.java、vue3/vue2Page.vue3...）
     */
    String generateCode(List<ColumnDetail> columnDetailList, String delPrefix, String packageName, String type);

    /**
     * 生成代码
     */
    public void generateCode ();
}
