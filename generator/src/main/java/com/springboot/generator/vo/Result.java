package com.springboot.generator.vo;//package com.springboot.generator.vo;

import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.NoArgsConstructor;
import lombok.ToString;
import java.io.Serializable;
import java.util.HashMap;

/**
 * @author liuc
 * @apiNote 统一结果返回
 * @date 2024/1/17 13:09
 */
@NoArgsConstructor
@ToString(callSuper = true)
@JsonInclude(JsonInclude.Include.NON_NULL)
public class Result<T> extends HashMap<String, Object> implements Serializable {

    private static final long serialVersionUID = 2637614641937282252L;

    private Integer code;
    private String message;
    private T data;

    public Result(Integer code, String message, T data, boolean flag) {
        this.code = code;
        this.message = message;
        this.data = data;
        put("code", code);
        put("msg", message);
        put("data", data);
    }

    public static <T> Result<T> ok(Integer code, String msg, T data) {
        return new Result<>(code, msg, data, true);
    }

    public static <T> Result<T> ok(String msg, T data) {
        return new Result<>(200, msg, data, true);
    }

    public static <T> Result<T> ok(T data) {
        return new Result<>(200, "操作成功！", data, true);
    }

    public static <T> Result<T> ok() {
        return new Result<>(200, "操作成功！", null, true);
    }

    public static <T> Result<T> fail(Integer code, String msg) {
        return new Result<>(code, msg, null, false);
    }

    public static <T> Result<T> fail(String msg) {
        return new Result<>(500, msg, null, false);
    }

    public static <T> Result<T> set(String key, Object value) {
        return set(key, value, init());
    }

    public static <T> Result<T> success(T data) {
        return ok(data);
    }

    public Result<T> add(String key, Object value) {
        return set(key, value);
    }

    public static <T> Result<T> init() {
        return new Result<>(null, null, null, true);
    }

    private static <T> Result<T> set(String key, Object value, Result<T> Result) {
        Result.put(key, value);
        return Result;
    }
}
