package com.springboot.generator.controller.code;

import cn.hutool.core.util.StrUtil;
import com.springboot.generator.entity.code.ColumnDetail;
import com.springboot.generator.service.code.ICodeGeneratorService;
import com.springboot.generator.vo.Result;
import jakarta.annotation.Resource;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.*;
import java.util.List;

/**
 * @author liuc
 * @apiNote 代码生成器控制层
 * @description 代码生成器控制层
 * @date 2024/1/17 12:43
 */
@Slf4j
@RestController
@RequiredArgsConstructor
@RequestMapping("/api/v1/gen/code")
public class CodeGeneratorController {
    @Resource
    ICodeGeneratorService codeGeneratorService;


    /**
     * 获取所有表信息数据
     *
     * @return List<ColumnDetail>表数据
     */
    @GetMapping("/getAllTables")
    public Result<List<ColumnDetail>> getAllTables() {
        List<ColumnDetail> ColumnDetailList = codeGeneratorService.getAllTableInfo();
        return Result.success(ColumnDetailList);
    }

    /**
     * 生成代码
     *
     * @param tableName   表名（tb_user）
     * @param delPrefix   需要去除的前缀（tb_）
     * @param packageName 文件所在包的包名（cn.molu.generator）
     * @param type        生成类型（java/entity.java、vue3/vue2Page.vue3...）
     * @return String代码
     */
    @GetMapping("/generate/{tableName}")
    public Result<String> getTableInfo(@PathVariable String tableName,
                                         @RequestParam(value = "delPrefix", required = false) String delPrefix,
                                         @RequestParam(value = "packageName", required = false) String packageName,
                                         @RequestParam(value = "type") String type
    ) {
        if (StrUtil.isBlank(tableName) || StrUtil.isBlank(type)) return Result.fail("参数为空");
        List<ColumnDetail> columnDetailList = codeGeneratorService.getColumnDetails(tableName);
        String code = codeGeneratorService.generateCode(columnDetailList, delPrefix, packageName, type);
        System.out.println(code);
        return Result.success(code);
    }

}

