package com.springboot.generator.mapper.basic;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.springboot.generator.entity.basic.SysRole;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author liuc
 * @since 2024-02-01
 */
@Mapper
public interface SysRoleMapper extends BaseMapper<SysRole> {

}
