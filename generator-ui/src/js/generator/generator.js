import request from "@/util/request.js";

//获取所有表信息
export function getAllTables(){
    return request.get('/api/v1/gen/code/getAllTables');
}