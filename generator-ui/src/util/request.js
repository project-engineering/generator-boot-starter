//定制请求的实例

//导入axios npm install axios
import axios from "axios";
//定义一个变量，记录公共的前缀，baseURL
const baseURL = 'http://localhost:8080';
const instance = axios.create({baseURL})

//添加响应拦截器
instance.interceptors.response.use(
    result=>{
       return result.data;
    },
    error => {
        alert('服务异常')
        //异步的状态转化成失败的状态
        return Promise.reject(error);
    }
)

export default instance;